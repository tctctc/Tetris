# Android版本的俄罗斯方块

##1. 实现的功能
- 方块的旋转、下降、左右移动
- 游戏的得分、游戏level的升级
- 游戏暂停、开始、结束、重新开始

##2. 成果展示

<img src="screen_shot/Screenshot_2.png" width = "180" height = "360" alt="图片名称" align=center />
<img src="screen_shot/Screenshot_1.png" width = "180" height = "360" alt="图片名称" align=center />

[视频连接](screen_shot/俄罗斯方块.mp4)