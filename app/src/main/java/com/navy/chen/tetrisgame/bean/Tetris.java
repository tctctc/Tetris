package com.navy.chen.tetrisgame.bean;

import android.util.Log;

import com.navy.chen.tetrisgame.tool.Config;
import com.navy.chen.tetrisgame.tool.Tools;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/11/27.
 */
public class Tetris implements Cloneable{
    public static final int type_sum = 7;//7种类型


    public static final int type_1 = 0;
    public static final int type_2 = 1;
    public static final int type_3 = 2;
    public static final int type_4 = 3;
    public static final int type_5 = 4;
    public static final int type_6 = 5;
    public static final int type_7 = 6;
    private final String TAG = Config.TAG+getClass().getSimpleName();

    public static Map<Integer, int[][]> tetrisMap = new HashMap<>();


    public static final int LEFT = -1;

    public static final int RIGHT = 1;

    static {

        //L S I O T J Z

        int [][]tetris1 = new int[][]{
                { 0, 0, 0, 0},
                { 1, 1, 1, 1},
                { 0, 0, 0, 0},
                { 0, 0, 0, 0}
        };

        int [][]tetris2 = new int[][]{
                { 1, 0, 0},
                { 1, 1, 1},
                { 0, 0, 0}
        };
        int [][]tetris3 =  new int[][]{
                { 0, 0, 1},
                { 1, 1, 1},
                { 0, 0, 0}
        };
        int [][]tetris4 = new int[][]{
                { 1, 1},
                { 1, 1}
        };
        int [][]tetris5 = new int[][]{
                { 0, 1, 1},
                { 1, 1, 0},
                { 0, 0, 0}
        };
        int [][]tetris6 = new int[][]{
                { 1, 1, 1},
                { 0, 1, 0},
                { 0, 0, 0}
        };


        int [][]tetris7 =  new int[][]{
                { 1, 1, 0},
                { 0, 1, 1},
                { 0, 0, 0}
        };

        tetrisMap.put(type_1, tetris1);
        tetrisMap.put(type_2, tetris2);
        tetrisMap.put(type_3, tetris3);
        tetrisMap.put(type_4, tetris4);
        tetrisMap.put(type_5, tetris5);
        tetrisMap.put(type_6, tetris6);
        tetrisMap.put(type_7, tetris7);

    }

    private int color;//真实的颜色
    private int type;

    private float x = 0; //x坐标
    private float y = 0; //y坐标


    private int matrix[][];

    public int[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }


    public static Tetris randomTetris(){
        Tetris tetris = new Tetris();

        int type = (int) (Math.random()*type_sum);
        tetris.color = Config.getInstance().getTetrisColors()[type];
        tetris.type = type;
        int [][]matrix = tetrisMap.get(type);
        //完成方块生产的多样性  0表示当前方向，1表示右旋转90度，2表示旋转180度，3表示左旋转90度
        int diretion = (int)(Math.random()*4);
        if (diretion>0){
            for (int i=0; i<diretion; i++){
               matrix= Tools.rotateMatrixRight(matrix);
            }
        }
        tetris.setMatrix(matrix);
        tetris.x = getStartX(tetris.matrix.length);
        tetris.y = 0;
        return tetris;
    }

    public static float getStartX(int length){
        int center =Config.getInstance().width_num/2;
        int half = length/2;
        if (length % 2 !=0){
            half += (Math.random()+0.5);
        }
        return (center-half)*Config.getInstance().getSideLength();
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "clone 错误");
        return null;
    }


    public int getRelativeX(float sideLength){
        int relativeX = 0;
        if (x>=0){
            relativeX = (int) (x / sideLength+0.5);
        } else{
            relativeX = (int) (x / sideLength-0.5);
        }
        return relativeX;
    }

    public int getRelativeY(float sideLength){
        return (int) (y / Config.getInstance().getSideLength()+0.5);//y一直大于0
    }
}