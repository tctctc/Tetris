package com.navy.chen.tetrisgame.interfaces;

/**
 * Created by Administrator on 2017/12/7.
 */
public interface GameControlListenr {
    public void speedUp();
    public void move(int Diretion);
    public void rotate();

    public void pauseGame();
    public void playGame();

    public void newGame();
}
