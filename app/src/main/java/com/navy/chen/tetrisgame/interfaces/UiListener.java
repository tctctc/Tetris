package com.navy.chen.tetrisgame.interfaces;

/**
 * Created by Administrator on 2017/12/7.
 */
public interface UiListener {
    /**
     * Created by Administrator on 2017/12/6.
     */

        public void onGameFinsih(int nowScoreSum);
        public void onGameScoresChange(int nowScoreSum);
        public void onGameLevelChange(int nowlevel);
        public void onGamePlayStateChange(boolean isPause);

}
