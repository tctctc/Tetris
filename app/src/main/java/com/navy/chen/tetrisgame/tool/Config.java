package com.navy.chen.tetrisgame.tool;

import android.util.Log;

/**
 * Created by Administrator on 2017/11/28.
 */
public class Config {
    private static Config ourInstance = new Config();



    public static Config getInstance() {
        return ourInstance;
    }

    public final int width_num = 10;
    public final int heght_num = 2*width_num;
    private float side_length = -1;

    private int colorBg;
    private int []tetrisColors;
    private int tetrisContentColor;




    public final static String TAG = "Tetris TAG: ";

    private Config() {
    }


    public void init(float side_length){
        Log.d(TAG, String.format("长度side_length = %f",side_length ));
        this.side_length = side_length;
    }

    public float getSideLength() {
        return side_length;
    }

    public int getColorBg() {
        return colorBg;
    }

    public void setColorBg(int colorBg) {
        this.colorBg = colorBg;
    }

    public int[] getTetrisColors() {
        return tetrisColors;
    }

    public void setTetrisColors(int[] tetrisColors) {
        this.tetrisColors = tetrisColors;
    }

    public int getTetrisContentColor() {
        return tetrisContentColor;
    }

    public void setTetrisContentColor(int tetrisContentColor) {
        this.tetrisContentColor = tetrisContentColor;
    }
}
