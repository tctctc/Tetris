package com.navy.chen.tetrisgame.tool;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.nfc.Tag;
import android.util.Log;

import com.navy.chen.tetrisgame.bean.Tetris;

/**
 * Created by Administrator on 2017/12/5.
 */
public class DrawHelper {
    private static DrawHelper ourInstance = new DrawHelper();
    private final String TAG = Config.TAG + getClass().getSimpleName();
    public static DrawHelper getInstance() {
        return ourInstance;
    }

    private DrawHelper() {
    }



    public void drawBg(Canvas canvas,int color, int widthNum, int heightNum, float side_length){
        Paint paint = new Paint();
        paint.setColor(Config.getInstance().getColorBg());
        //画竖
        canvas.drawColor(Config.getInstance().getTetrisContentColor());
        for (int i=0; i<=widthNum; i++){
            canvas.drawLine(i*side_length ,0, i*side_length, heightNum*side_length,paint );
        }
        for (int i=0; i<=heightNum; i++){
            canvas.drawLine(0, i*side_length,widthNum*side_length, i*side_length, paint );
        }
    }

    public void drawATetris(Tetris tetris, Canvas canvas, float sideLength) {
        int matrix[][] = tetris.getMatrix();
        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[j][i] != 0) {
                    float x = tetris.getX() + i * sideLength;
                    float y = tetris.getY() + j * sideLength;
                    RectF rectangle = new RectF(x, y, x + sideLength, y + sideLength);
                    DrawHelper.getInstance().drawRectangle(canvas, rectangle, tetris.getColor());
                }
            }
        }
    }

    public void clearRectangle(Canvas canvas, Bitmap bitmap,RectF rectangle){

        float offset = rectangle.bottom - rectangle.top;
        float w= Config.getInstance().getSideLength()*Config.getInstance().width_num;
        float h= Config.getInstance().getSideLength()*Config.getInstance().heght_num;
        Bitmap temp = Bitmap.createBitmap((int)w,(int)h, Bitmap.Config.ARGB_8888);
        Canvas canvasTemp = new Canvas(temp);
        Rect scr = new Rect(0, 0,(int) w, (int)rectangle.top);
        Paint paint = new Paint();
        canvasTemp.drawBitmap(bitmap, scr,
                new RectF(scr.left, scr.top+offset, scr.right, scr.bottom+offset), paint);
        Rect scr2 = new Rect(0,(int) rectangle.bottom,(int) w,(int) rectangle.bottom);//顶部的内容
        canvasTemp.drawBitmap(bitmap, scr,
                new RectF(scr2.left, scr2.top, scr2.right, scr2.bottom), paint);
        canvas = new Canvas(bitmap);
        canvas.drawBitmap(temp,0,0,paint);

    }

    private void drawRectangle(Canvas canvas, RectF rect, int color) {
        Paint p = new Paint();
        p.setColor(color);
        p.setStyle(Paint.Style.FILL);
        canvas.drawRect(rect, p);
        Log.d(TAG, String.format("rect w =%f h=%f", rect.right-rect.left, rect.bottom-rect.top));
        Paint paint = new Paint();
        paint.setColor(Config.getInstance().getColorBg());
        canvas.drawLine(rect.left, rect.top, rect.right, rect.top, paint);
        canvas.drawLine(rect.left, rect.bottom, rect.right, rect.bottom, paint);
        canvas.drawLine(rect.left, rect.top, rect.left, rect.bottom, paint);
        canvas.drawLine(rect.right, rect.top, rect.right, rect.bottom, paint);
    }
}
