package com.navy.chen.tetrisgame.tool;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.navy.chen.tetrisgame.tool.Config;

/**
 * Created by Administrator on 2017/12/1.
 */
public class Tools {

    private static  final String TAG = Config.TAG;

    @TargetApi(Build.VERSION_CODES.M)
    public static final int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return context.getColor(id);
        } else {
            return context.getResources().getColor(id);
        }
    }


    public static void print(int arr[][]){
        if (arr !=null) {
            for (int i=0; i<arr.length; i++){
                StringBuffer sb = new StringBuffer();
                for (int j=0; j<arr[0].length; j++){
                    sb.append("" +arr[i][j]+",");
                }
                Log.d(TAG, sb.toString());
            }
        }
    }

    public static int getBlankLeft(int matrix[][]){
        if (matrix == null){
            return 0;
        }
        for (int i=0; i<matrix[0].length; i++){
            for (int j=0; j<matrix.length; j++){
                if (matrix[j][i] != 0) {
                    return i;
                }
            }
        }
        return matrix[0].length;
    }

    public static int getBlankTop(int matrix[][]){
        if (matrix == null){
            return 0;
        }
        for (int i=0; i<matrix.length; i++){
            for (int j=0; j<matrix[0].length; j++){
                if (matrix[i][j] != 0){
                    return i;
                }
            }
        }
        return matrix.length;
    }

    public static int getBlankBottom(int matrix[][]){
        if (matrix == null){
            return 0;
        }
        for (int i=matrix.length-1; i>=0; i--){
            for (int j=0; j<matrix[0].length; j++){
                if (matrix[i][j] != 0){

                    return matrix.length-i-1;
                }
            }
        }
        return matrix.length;
    }
    public static int getBlankRight(int matrix[][]){
        if (matrix == null){
            return 0;
        }
        for (int i=matrix[0].length-1; i>=0; i--){
            for (int j=0; j<matrix.length; j++){
                if (matrix[j][i] != 0){
                    return  matrix[0].length-i-1;
                }
            }
        }
        return matrix[0].length;
    }

    public static int[][] rotateMatrixRight(int matrix[][]){
        if (matrix == null){
            return null;
        }
        int[][] temp = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                temp[i][j]=matrix[matrix[i].length-j-1][i];
            }
        }
        return temp;
    }
    public static int[][] rotateMatrixLeft(int matrix[][]){
        if (matrix == null){
            return null;
        }
        int[][] temp = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                temp[i][j]=matrix[j][matrix[j].length-1-i];
            }
        }
        return temp;
    }


    public static void moveDownMatrixContent(int matrix[][], int start, int end){
        if (matrix == null){
            return;
        }
        int offset = end - start+1;
        for (int i= start-1; i>=0; i--){
            for (int j=0;j<matrix[0].length;j++){
                matrix[i+offset][j] = matrix[i][j];
            }
        }
        //由于本俄罗斯方块 0到offset -1的行的内容为0； 所以不辅助值了
    }

}
