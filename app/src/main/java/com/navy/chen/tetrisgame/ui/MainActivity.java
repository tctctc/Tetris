package com.navy.chen.tetrisgame.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.navy.chen.tetrisgame.bean.Tetris;
import com.navy.chen.tetrisgame.interfaces.GameControlListenr;
import com.navy.chen.tetrisgame.interfaces.UiListener;
import com.navy.chen.tetrisgame.tool.Config;
import com.navy.chen.tetrisgame.R;
import com.navy.chen.tetrisgame.tool.Tools;
import com.navy.chen.tetrisgame.interfaces.ProductListener;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, UiListener {

    private final String TAG = Config.TAG+getClass().getSimpleName();

    private TetrisView tetrisView;
    private TetrisPreview tetrisPreview;
    private Button left;
    private Button right;
    private Button down;
    private Button turn_left;
    private Button turn_right;

    private TextView tv_score;
    private TextView tv_hi_score;
    private TextView tv_level;
    private CheckBox checkBox_play;


    private static final String highScore = "high_score";
    private static final String configName = "config";
    private int hiScore = 0;
    GameControlListenr controlListenr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initConfig();
        setContentView(R.layout.act_main);
        init();
        setListener();

    }




    private void init(){
        tetrisView = (TetrisView) findViewById(R.id.tetrisView);
        tetrisPreview = (TetrisPreview) findViewById(R.id.tetrisPreviews);
        left = (Button) findViewById(R.id.btn_left);
        right = (Button) findViewById(R.id.btn_right);
        down = (Button) findViewById(R.id.btn_down);
        turn_left = (Button) findViewById(R.id.btn_turn_left);
        turn_right = (Button) findViewById(R.id.btn_turn_right);

        tv_score = (TextView) findViewById(R.id.tv_score);
        tv_hi_score = (TextView) findViewById(R.id.tv_hi_score);
        tv_level = (TextView) findViewById(R.id.tv_level);
        checkBox_play = (CheckBox) findViewById(R.id.checkbox_play);
        controlListenr = tetrisView.getGameControlListenr();
        SharedPreferences sp  = getSharedPreferences(configName,MODE_WORLD_READABLE );
        hiScore = sp.getInt(highScore, 0);
        setText(tv_hi_score, hiScore);


    }
    private void setListener() {
        left.setOnClickListener(this);
        right.setOnClickListener(this);
        down.setOnClickListener(this);
        turn_left.setOnClickListener(this);
        turn_right.setOnClickListener(this);

        tetrisView.setTetrisUiListener(this);
        tetrisView.setProductListener(tetrisPreview);

        checkBox_play.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    controlListenr.playGame();
                } else {
                    controlListenr.pauseGame();
                }
            }
        });
    }

    private void initConfig(){

        Config.getInstance().setColorBg(Tools.getColor(this, R.color.bg));
        int color1 = Tools.getColor(this, R.color.tetris_1);
        int color2 = Tools.getColor(this, R.color.tetris_2);
        int color3 = Tools.getColor(this, R.color.tetris_3);
        int color4 = Tools.getColor(this, R.color.tetris_4);
        int color5 = Tools.getColor(this, R.color.tetris_5);
        int color6 = Tools.getColor(this, R.color.tetris_6);
        int color7 = Tools.getColor(this, R.color.tetris_7);
        int[] colors = new int[]{color1,color2,color3,color4,color5,color6,color7};
        Config.getInstance().setTetrisColors(colors);
        Config.getInstance().setTetrisContentColor(Tools.getColor(this, R.color.tetris_content_bg));
        Log.d(TAG, "Config 颜色初始化完成");
}


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case  R.id.btn_down:
                controlListenr.speedUp();
                break;
            case R.id.btn_left:
                controlListenr.move(Tetris.LEFT);
                break;
            case R.id.btn_right:
                controlListenr.move(Tetris.RIGHT);
                break;
            case R.id.btn_turn_left:
                break;
            case R.id.btn_turn_right:
                controlListenr.rotate();
                break;
        }

    }

    @Override
    public void onGameFinsih(int nowScoreSum) {

        final AlertDialog.Builder dialog =
                new AlertDialog.Builder(MainActivity.this);
        dialog.setTitle(R.string.game_title);
        dialog.setCancelable(false);
        dialog.setMessage(String.format("你一共得了%d分,请加油",nowScoreSum));
        dialog.setPositiveButton("开始",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //...To-do
                    }
                });
        dialog.setNegativeButton("退出",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.finish();
                    }
                });
        // 显示
       runOnUiThread(new Runnable() {
           @Override
           public void run() {
               dialog.show();
           }
       });

    }

    @Override
    public void onGameScoresChange(final int nowScoreSum) {


        setText(tv_score, nowScoreSum);
        if (nowScoreSum > hiScore){
            SharedPreferences.Editor editor = getSharedPreferences(configName,MODE_WORLD_WRITEABLE).edit();
            editor.putInt(highScore, nowScoreSum);
            editor.apply();
            hiScore = nowScoreSum;
            setText(tv_hi_score, nowScoreSum);

        }

    }

    @Override
    public void onGameLevelChange(final int nowlevel) {

        setText(tv_level, nowlevel);

    }

    @Override
    public void onGamePlayStateChange(final boolean isPause) {
        checkBox_play.post(new Runnable() {
            @Override
            public void run() {
                checkBox_play.setChecked(!isPause);
            }
        });
    }

    public void setText(final TextView tv,final int content){
        tv.post(new Runnable() {
            @Override
            public void run() {
                DecimalFormat df = new DecimalFormat("0000");
                tv.setText(df.format(content));
            }
        });
    }
}
