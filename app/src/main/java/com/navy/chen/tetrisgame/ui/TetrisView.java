package com.navy.chen.tetrisgame.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.nfc.Tag;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.navy.chen.tetrisgame.bean.Tetris;
import com.navy.chen.tetrisgame.interfaces.GameControlListenr;
import com.navy.chen.tetrisgame.interfaces.UiListener;
import com.navy.chen.tetrisgame.tool.Config;
import com.navy.chen.tetrisgame.thread.GameThread;
import com.navy.chen.tetrisgame.interfaces.ProductListener;

/**
 * Created by Administrator on 2017/12/2.
 */
public class TetrisView extends SurfaceView implements SurfaceHolder.Callback{

    private final String TAG = Config.TAG+getClass().getSimpleName();

    private SurfaceHolder surfaceHolder;
    private GameThread thread;
    private boolean isInitThread = false;
    private Bitmap bitmap = null;
    private boolean isPause = false;
    public TetrisView(Context context) {
        super(context);
    }



    public TetrisView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setKeepScreenOn(true);
        this.setFocusable(true);
        //用于触摸事件
        this.setLongClickable(true);
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        thread = new GameThread(surfaceHolder);
    }



    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated");


    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "surfaceChanged");
        //保存界面信息
        if (bitmap == null){
          bitmap=  Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        }
        Canvas canvas = holder.lockCanvas();
        canvas.drawBitmap(bitmap, 0, 0, new Paint());
        holder.unlockCanvasAndPost(canvas);
        thread.setBitmap(bitmap);

        if (!isInitThread){
            thread.start();
            isInitThread = true;
        }
        if (isPause){
            thread.getGameControlListenr().pauseGame();
        } else {
            thread.getGameControlListenr().playGame();
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "surfaceDestroyed");
        isPause = thread.isPause;
        thread.getGameControlListenr().pauseGame();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int w = getMeasuredWidth();
        Config.getInstance().init(w  / Config.getInstance().width_num);

        setMeasuredDimension(w, 2*w);
    }


    public void setTetrisUiListener(UiListener uiListener){
        thread.setUiListener(uiListener);
    }
    public void setProductListener(ProductListener productListener) {
        thread.setProductListener(productListener);
    }

    public GameControlListenr getGameControlListenr() {
        return thread.getGameControlListenr();
    }

}
